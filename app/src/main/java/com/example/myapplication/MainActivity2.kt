package com.example.myapplication


import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.FileProvider
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class MainActivity2 : AppCompatActivity() {
    private lateinit var myRootView:View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
        // this is the card view whose screenshot
        // we will take in this article
        // get the view using fin view bt id
        myRootView = findViewById<CardView>(R.id.card_View)

        // on click of this button it will capture
        // screenshot and save into gallery
        val captureButton = findViewById<Button>(R.id.btn_capture)
        captureButton.setOnClickListener {
            // get the bitmap of the view using
            // getScreenShotFromView method it is
            // implemented below
            val bitmap = getScreenShotFromView(myRootView)

            // if bitmap is not null then
            // save it to gallery
            if (bitmap != null) {
               // saveMediaToStorage(bitmap)
              val path= getImageUri(this,bitmap)

                if (path!=null){

                    val intent = Intent(Intent.ACTION_SEND).setType("image/*")



                    // Step 6: Put Uri as extra to share intent
                    intent.putExtra(Intent.EXTRA_STREAM, path)

                    // Step 7: Start/Launch Share intent
                    startActivity(intent)

                }
            }






        }


    }


    private fun getScreenShotFromView(v: View): Bitmap? {
        // create a bitmap object
        var screenshot: Bitmap? = null
        try {
            // inflate screenshot object
            // with Bitmap.createBitmap it
            // requires three parameters
            // width and height of the view and
            // the background color
            screenshot = Bitmap.createBitmap(v.measuredWidth, v.measuredHeight, Bitmap.Config.ARGB_8888)
            // Now draw this bitmap on a canvas
            val canvas = Canvas(screenshot)
            v.draw(canvas)
        } catch (e: Exception) {
            Log.e("GFG", "Failed to capture screenshot because:" + e.message)
        }
        // return the bitmap
        return screenshot
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(
            inContext.getContentResolver(),
            inImage,
            "Title",
            null
        )
        return Uri.parse(path)
    }


    // this method saves the image to gallery
    private fun saveMediaToStorage(bitmap: Bitmap) {
        // Generating a file name
        val filename = "${System.currentTimeMillis()}.jpg"

        // Output stream
        var fos: OutputStream? = null

        // For devices running android >= Q
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            // getting the contentResolver
            this.contentResolver?.also { resolver ->

                // Content resolver will process the contentvalues
                val contentValues = ContentValues().apply {

                    // putting file information in content values
                    put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                }

                // Inserting the contentValues to
                // contentResolver and getting the Uri
                val imageUri: Uri? = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

                // Opening an outputstream with the Uri that we got
                fos = imageUri?.let { resolver.openOutputStream(it) }



            }
        } else {
            // These for devices running on android < Q
            val imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val image = File(imagesDir, filename)
            fos = FileOutputStream(image)
        }

        fos?.use {
            // Finally writing the bitmap to the output stream that we opened
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
            Toast.makeText(this , "Captured View and saved to Gallery" , Toast.LENGTH_SHORT).show()
        }
    }





    private fun shareScreenshot() {
        // We need date and time to be added to image name to make it unique every time, otherwise bitmap will not update
        val sdf = SimpleDateFormat("yyyyMMdd_HHmmss")
        val currentDateandTime = sdf.format(Date())
        val imageName = "/image_$currentDateandTime.jpg"

        // CREATE
        myRootView = window.decorView.rootView

        myRootView.isDrawingCacheEnabled = true
        myRootView.buildDrawingCache(true) // maybe You dont need this
        val bitmap = Bitmap.createBitmap(myRootView.drawingCache)
        myRootView.isDrawingCacheEnabled = false

        // SAVE
        try {
            File(this.cacheDir, "images").deleteRecursively() // delete old images
            val cachePath = File(this.cacheDir, "images")
            cachePath.mkdirs() // don't forget to make the directory
            val stream = FileOutputStream("$cachePath$imageName")
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream) // can be png and any quality level
            stream.close()
        } catch (ex: Exception) {
            Toast.makeText(this, ex.javaClass.canonicalName, Toast.LENGTH_LONG).show() // You can replace this with Log.e(...)
        }

        // SHARE
        val imagePath = File(this.cacheDir, "images")
        val newFile = File(imagePath, imageName)
        val contentUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", newFile)
        if (contentUri != null) {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION) // temp permission for receiving app to read this file
            shareIntent.type = "image/jpeg" // just assign type. we don't need to set data, otherwise intent will not work properly
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
            startActivity(Intent.createChooser(shareIntent, "Choose app"))
        }



         fun showShareIntent( path: String) {
            // Step 1: Create Share itent
            val intent = Intent(Intent.ACTION_SEND).setType("image/*")



            // Step 6: Put Uri as extra to share intent
            intent.putExtra(Intent.EXTRA_STREAM, path)

            // Step 7: Start/Launch Share intent
            startActivity(intent)
        }
    }




}
